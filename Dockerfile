FROM openjdk:8-jdk as builder

COPY pom.xml mvnw /app/
COPY .mvn /app/.mvn
COPY src /app/src
WORKDIR /app/
RUN ./mvnw package -DskipTests=true

FROM openjdk:8-jdk

EXPOSE 80
VOLUME /tmp
COPY --from=builder /app/target/*.jar /app/app.jar
CMD [ "java", "-jar", "/app/app.jar" ]