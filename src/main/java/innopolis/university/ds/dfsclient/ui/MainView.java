package innopolis.university.ds.dfsclient.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.router.Route;
import innopolis.university.ds.dfsclient.model.DirectoryContent;
import innopolis.university.ds.dfsclient.model.File;
import innopolis.university.ds.dfsclient.model.FileInfo;
import innopolis.university.ds.dfsclient.rest.NameNodeController;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Route
@Log4j2
public class MainView extends HorizontalLayout {

    private final NameNodeController controller;

    @Value("${file.download_directory}")
    private String downloadPath;

    private static final String CONFIRM = "Confirm";
    private static final String CANCEL = "Cancel";
    private static final String FILE_PATTERN = "[a-zA-Z0-9_.]+";
    private static final String DIR_PATTERN = "[a-zA-Z0-9_]+";
    private static final String CURRENT_DIR_TEXT = "Current directory: ";
    private String currentDir = "/";

    private Grid<File> files = new Grid<>(File.class, false);
    private Label currentDirText = new Label(CURRENT_DIR_TEXT + currentDir);

    public MainView(NameNodeController controller) {
        this.controller = controller;
        Button dirUp = new Button(VaadinIcon.ARROW_UP.create(), buttonClickEvent -> {
            if (currentDir.equals("/")) {
                return;
            }
            List<String> dirs = new ArrayList<>(Arrays.asList(currentDir.split("/")));
            dirs.remove(dirs.size() - 1);
            currentDir = String.join("/", dirs) + '/';
            currentDirText.setText(CURRENT_DIR_TEXT + currentDir);
            files.setItems(getFiles());
        });
        Button createDir = new Button("Create directory",
                buttonClickEvent -> createDirPopup().open());
        Button createFile = new Button("Create file",
                buttonClickEvent -> createFilePopup().open());
        Button uploadFile = new Button("Upload file",
                buttonClickEvent -> uploadFilePopup().open());
        Button initSystem = new Button("Initialize system",
                buttonClickEvent -> initSystemPopup().open());
        Button copyFile = new Button("Copy file",
                buttonClickEvent -> copyFilePopup(files.asSingleSelect().getValue().getFullPath()).open());
        copyFile.setEnabled(false);
        Button moveFile = new Button("Move file",
                buttonClickEvent -> moveFilePopup(files.asSingleSelect().getValue().getFullPath()).open());
        moveFile.setEnabled(false);
        Button fileInfo = new Button("File info",
                buttonClickEvent -> fileInfoPopup(files.asSingleSelect().getValue().getFullPath()).open());
        fileInfo.setEnabled(false);
        Button delete = new Button("Delete",
                buttonClickEvent -> {
                    File current = files.asSingleSelect().getValue();
                    if (current.isDir()) {
                        deleteDirPopup(current.getFullPath()).open();
                    } else {
                        deleteFilePopup(current.getFullPath()).open();
                    }
                });
        delete.setEnabled(false);

        files.addComponentColumn(row -> row.isDir() ? VaadinIcon.FOLDER.create() : VaadinIcon.FILE.create());
        files.addColumn("name");
        files.setItems(getFiles());
        files.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                copyFile.setEnabled(false);
                moveFile.setEnabled(false);
                fileInfo.setEnabled(false);
                delete.setEnabled(false);
                return;
            }
            boolean isDir = event.getValue().isDir();
            copyFile.setEnabled(!isDir);
            moveFile.setEnabled(!isDir);
            fileInfo.setEnabled(!isDir);
            delete.setEnabled(true);
        });
        files.addItemDoubleClickListener(event -> {
            File selected = event.getItem();
            if (selected.isDir()) {
                currentDir = selected.getFullPath() + "/";
                currentDirText.setText(CURRENT_DIR_TEXT + currentDir);
                files.setItems(getFiles());
            } else {
                java.io.File file = Paths.get(downloadPath, selected.getName()).toFile();
                try {
                    Resource resource = controller.readFile(toString(selected.getFullPath()));
                    FileUtils.copyInputStreamToFile(resource.getInputStream(), file);
                } catch (IOException e) {
                    log.error("Cannot read the file.", e);
                }
            }
        });
        VerticalLayout dataColumn = new VerticalLayout();
        dataColumn.add(currentDirText);
        dataColumn.add(new Label("Current directory content:"));
        dataColumn.add(files);
        VerticalLayout buttonColumn = new VerticalLayout();
        buttonColumn.add(dirUp, createDir, createFile, uploadFile, copyFile, moveFile, fileInfo, delete, initSystem);
        add(dataColumn, buttonColumn);
    }

    private Dialog initSystemPopup() {
        VerticalLayout dialogBase = new VerticalLayout();
        Dialog dialog = new Dialog(dialogBase);
        Button confirm = new Button(CONFIRM, buttonClickEvent -> {
            long size = controller.initializeSystem();
            VerticalLayout notificationBase = new VerticalLayout();
            Notification notification = new Notification(notificationBase);
            notification.setPosition(Notification.Position.MIDDLE);
            notificationBase.add(new Label("The system initialized successfully, available size is: " + size),
                    new Button("OK", buttonClickEvent1 -> notification.close()));
            notification.open();
            files.setItems(getFiles());
            dialog.close();
        });
        return getLabeledDialog(dialogBase, dialog, confirm);
    }

    private Dialog uploadFilePopup() {
        VerticalLayout dialogBase = new VerticalLayout();
        Dialog dialog = new Dialog(dialogBase);
        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);

        Button confirm = new Button(CONFIRM, buttonClickEvent -> {
            byte[] fileContent = getBytes(buffer.getInputStream());
            controller.writeFile(toString(Paths.get(currentDir, buffer.getFileName())), fileContent);
            files.setItems(getFiles());
            dialog.close();
        });
        return getDialog(upload, dialogBase, dialog, confirm);
    }

    private byte[] getBytes(InputStream inputStream) {
        try {
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            log.error("Cannot read the file.", e);
        }
        return new byte[0];
    }

    private Dialog createDirPopup() {
        return createPopup("Directory name", DIR_PATTERN, controller::createDir);
    }

    private Dialog createFilePopup() {
        return createPopup("File name", FILE_PATTERN, controller::createFile);
    }

    private Dialog createPopup(String textFieldName, String pattern, Consumer<String> controllerMethod) {
        VerticalLayout dialogBase = new VerticalLayout();
        Dialog dialog = new Dialog(dialogBase);
        TextField name = new TextField(textFieldName);
        name.focus();
        name.setRequired(true);
        name.setPattern(pattern);
        name.setPreventInvalidInput(true);
        Button confirm = new Button(CONFIRM, buttonClickEvent -> {
            controllerMethod.accept(toString(Paths.get(currentDir, name.getValue())));
            files.setItems(getFiles());
            dialog.close();
        });
        return getDialog(name, dialogBase, dialog, confirm);
    }

    private Dialog getDialog(Component component, VerticalLayout dialogBase, Dialog dialog, Button confirm) {
        confirm.addClickShortcut(Key.ENTER);
        confirm.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button close = new Button(CANCEL, buttonClickEvent -> dialog.close());
        dialogBase.add(component);
        dialogBase.add(new HorizontalLayout(confirm, close));
        return dialog;
    }

    private Dialog copyFilePopup(String path) {
        return changePopup(path, controller::copyFile);
    }

    private Dialog moveFilePopup(String path) {
        return changePopup(path, controller::moveFile);
    }

    private Dialog changePopup(String path, BiConsumer<String, String> controllerMethod) {
        VerticalLayout dialogBase = new VerticalLayout();
        Dialog dialog = new Dialog(dialogBase);
        ComboBox<String> dirs = new ComboBox<>("Available directories");
        dirs.focus();
        dirs.setItems(getDirs());
        dirs.setClearButtonVisible(true);
        dirs.setRequired(true);
        TextField newFileName = new TextField("New file name");
        newFileName.setRequired(true);
        newFileName.setPattern(FILE_PATTERN);
        newFileName.setPreventInvalidInput(true);
        Button confirm = new Button(CONFIRM, buttonClickEvent -> {
            controllerMethod.accept(toString(path), toString(Paths.get(dirs.getValue(), newFileName.getValue())));
            files.setItems(getFiles());
            dialog.close();
        });
        confirm.addClickShortcut(Key.ENTER);
        confirm.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button close = new Button(CANCEL, buttonClickEvent -> dialog.close());
        dialogBase.add(new HorizontalLayout(dirs, newFileName));
        dialogBase.add(new HorizontalLayout(confirm, close));
        return dialog;
    }

    private Dialog deleteFilePopup(String path) {
        return deletePopup(path, controller::deleteFile);
    }

    private Dialog deleteDirPopup(String path) {
        return deletePopup(path, controller::deleteDir);
    }

    private Dialog deletePopup(String path, Consumer<String> controllerMethod) {
        VerticalLayout dialogBase = new VerticalLayout();
        Dialog dialog = new Dialog(dialogBase);
        Button confirm = new Button(CONFIRM, buttonClickEvent -> {
            controllerMethod.accept(toString(path));
            files.setItems(getFiles());
            dialog.close();
        });
        return getLabeledDialog(dialogBase, dialog, confirm);
    }

    private Dialog getLabeledDialog(VerticalLayout dialogBase, Dialog dialog, Button confirm) {
        confirm.addClickShortcut(Key.ENTER);
        confirm.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button close = new Button(CANCEL, buttonClickEvent -> dialog.close());
        dialogBase.add(new Label("Are you sure?"));
        dialogBase.add(new HorizontalLayout(confirm, close));
        return dialog;
    }

    private Dialog fileInfoPopup(String path) {
        VerticalLayout dialogBase = new VerticalLayout();
        Dialog dialog = new Dialog(dialogBase);
        FileInfo fileInfo = controller.getFileInfo(toString(path));
        Label size = new Label("Size: " + fileInfo.getSize());
        Label creationDate = new Label(
                "Creation date: " + fileInfo.getCreationDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")));
        Button close = new Button("OK", buttonClickEvent -> dialog.close());
        dialogBase.add(size, creationDate);
        dialogBase.add(close);
        return dialog;
    }

    private Stream<File> getFiles() {
        DirectoryContent directoryContent = controller.readDir(toString(Paths.get(currentDir)));
        return Stream.concat(directoryContent.getDirs().stream().map(dir -> {
                    dir = '/' + dir;
                    return new File(true, getCurrentFileName(dir), dir);
                }),
                directoryContent.getFiles().stream().map(file -> {
                    file = '/' + file;
                    return new File(false, getCurrentFileName(file), file);
                }));
    }

    private String getCurrentFileName(String path) {
        int startIndex = currentDir.length();
        int endIndex = path.indexOf('/', startIndex);
        return endIndex < 0 ? path.substring(startIndex) : path.substring(startIndex, endIndex);
    }

    private Stream<String> getDirs() {
        return controller.readDirs().getDirs().stream().map(path -> "/" + path);
    }

    private String toString(Path path) {
        return path.toString().replaceFirst("/", "");
    }

    private String toString(String path) {
        return path.replaceFirst("/", "");
    }
}
