package innopolis.university.ds.dfsclient.rest;

import innopolis.university.ds.dfsclient.model.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static innopolis.university.ds.dfsclient.rest.Endpoints.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA;

@Log4j2
@Service
public class NameNodeController {

    @Value("http://" + "${namenode-host}")
    private String nameNodeHost;

    public Directories readDirs() {
        RestTemplate restTemplate = new RestTemplate();
        return Optional.ofNullable(restTemplate.getForObject(
                URI.create(nameNodeHost + DIRS_ALL),
                Directories.class
        )).orElseThrow(RuntimeException::new);
    }

    public DirectoryContent readDir(String currentDir) {
        RestTemplate restTemplate = new RestTemplate();
        return Optional.ofNullable(restTemplate.getForObject(
                UriComponentsBuilder.fromUriString(nameNodeHost + DIRS).queryParam("path", currentDir).build().toUri(),
                DirectoryContent.class
        )).orElseThrow(RuntimeException::new);
    }

    public void deleteDir(String path) {
        deleteMethod(path, DIRS);
    }

    public void createDir(String path) {
        createMethod(path, DIRS);
    }

    public Resource readFile(String path) {
        RestTemplate restTemplate = new RestTemplate();
        return Optional.ofNullable(restTemplate.getForObject(
                UriComponentsBuilder.fromUriString(nameNodeHost + FILES).queryParam("path", path).build().toUri(),
                Resource.class
        )).orElseThrow(RuntimeException::new);
    }

    public void writeFile(String path, byte[] bytes) {
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION,
                ContentDisposition
                        .builder("form-data")
                        .name("file")
                        .filename(path)
                        .build().toString());

        body.add("file", new HttpEntity<>(bytes, fileMap));
        body.add("path", path);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MULTIPART_FORM_DATA);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity(
                URI.create(nameNodeHost + FILES),
                new HttpEntity<>(body, headers),
                String.class
        );

        if (!response.getStatusCode().is2xxSuccessful()) {
            log.error(response.getStatusCode().getReasonPhrase(), response.getBody());
        }
    }

    public void createFile(String path) {
        createMethod(path, FILES_CREATE);
    }

    private void createMethod(String path, String url) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        ResponseEntity<String> response = restTemplate.postForEntity(
                URI.create(nameNodeHost + url),
                new HttpEntity<>(new Path(path), headers),
                String.class
        );

        if (!response.getStatusCode().is2xxSuccessful()) {
            log.error(response.getStatusCode().getReasonPhrase(), response.getBody());
        }
    }

    public void moveFile(String path, String newPath) {
        changeFile(path, newPath, HttpMethod.PATCH);
    }

    public void copyFile(String path, String newPath) {
        changeFile(path, newPath, HttpMethod.PUT);
    }

    private void changeFile(String path, String newPath, HttpMethod method) {
        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> response = restTemplate.exchange(
                UriComponentsBuilder.fromUriString(nameNodeHost + FILES).queryParam("path", path).build().toUri(),
                method,
                new HttpEntity<>(new NewPath(newPath), headers),
                String.class
        );

        if (!response.getStatusCode().is2xxSuccessful()) {
            log.error(response.getStatusCode().getReasonPhrase(), response.getBody());
        }
    }

    public void deleteFile(String path) {
        deleteMethod(path, FILES);
    }

    private void deleteMethod(String path, String url) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(
                UriComponentsBuilder.fromUriString(nameNodeHost + url).queryParam("path", path).build().toUri()
        );
    }

    public FileInfo getFileInfo(String path) {
        RestTemplate restTemplate = new RestTemplate();
        return Optional.ofNullable(restTemplate.getForObject(
                UriComponentsBuilder.fromUriString(nameNodeHost + FILES_INFO).queryParam("path", path).build().toUri(),
                FileInfo.class
        )).orElseThrow(RuntimeException::new);
    }

    public long initializeSystem() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Long> response = restTemplate.postForEntity(
                URI.create(nameNodeHost + INIT),
                null,
                Long.class
        );

        if (!response.getStatusCode().is2xxSuccessful()) {
            log.error(response.getStatusCode().getReasonPhrase(), response.getBody());
        }

        return Optional.ofNullable(response.getBody()).orElse(-1L);
    }
}
