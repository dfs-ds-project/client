package innopolis.university.ds.dfsclient.rest;

public class Endpoints {

    private Endpoints() {
    }

    public static final String SYSTEM = "/system";
    public static final String INIT = SYSTEM + "/init";
    public static final String FILE_SYSTEM = "/filesystem";
    public static final String DIRS = FILE_SYSTEM + "/dirs";
    public static final String DIRS_ALL = DIRS + "/all";
    public static final String FILES = FILE_SYSTEM + "/files";
    public static final String FILES_CREATE = FILES + "/create";
    public static final String FILES_INFO = FILES + "/info";
}
