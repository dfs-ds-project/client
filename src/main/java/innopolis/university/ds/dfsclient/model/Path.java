package innopolis.university.ds.dfsclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Path {

    @JsonProperty("path")
    private String path;
}
