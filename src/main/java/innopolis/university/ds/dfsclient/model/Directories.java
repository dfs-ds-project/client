package innopolis.university.ds.dfsclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Collection;

@Data
public class Directories {

    @JsonProperty("dirs")
    private Collection<String> dirs;
}
