package innopolis.university.ds.dfsclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class DirectoryContent {

    @JsonProperty("dirs")
    private List<String> dirs;

    @JsonProperty("files")
    private List<String> files;
}
