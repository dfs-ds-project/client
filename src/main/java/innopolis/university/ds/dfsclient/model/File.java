package innopolis.university.ds.dfsclient.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class File {

    private boolean isDir;

    private String name;

    private String fullPath;
}
