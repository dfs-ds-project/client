package innopolis.university.ds.dfsclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DfsClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(DfsClientApplication.class, args);
    }

}
